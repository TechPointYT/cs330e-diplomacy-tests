#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Aeslyn N. Kail
# Eiler J.R. Schiotz
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Game, Army, diplomacy_read, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):

    # -------------------------------------------------------
    # game class tests
    # -------------------------------------------------------
    def test_game_methods_1(self):
        # set up
        game_object = Game()
        army_object_A = Army("A", "Madrid", "Hold")
        game_object.add_army(army_object_A)
        army_object_B = Army("B", "Barcelona", "Move", "Madrid")
        game_object.add_army(army_object_B)
        army_object_D = Army("D", "Paris", "Support", "B")
        game_object.add_army(army_object_D)
        self.assertEqual(game_object.army_list, [
                         army_object_A, army_object_B, army_object_D])

        # run actions
        game_object.run_actions()
        # write test for do hold
        self.assertEqual(army_object_A.status_final, "Madrid")
        # tests do_move internally
        self.assertEqual(army_object_B.status_final, "Madrid")
        # tests do_support internally
        self.assertEqual(army_object_D.status_final, "Paris")
        self.assertEqual(army_object_B.support, [army_object_D])

        # eval game
        # tests find conflicts and resolve conflicts internally
        game_object.eval_game()
        self.assertEqual(army_object_A.status_final, "[dead]")
        self.assertEqual(army_object_B.status_final, "Madrid")
        self.assertEqual(army_object_D.status_final, "Paris")

    def test_game_methods_2(self):
        # set up
        game_object = Game()
        army_object_A = Army("A", "Austin", "Hold")
        game_object.add_army(army_object_A)
        self.assertEqual(game_object.army_list, [army_object_A])

        # run actions
        game_object.run_actions()
        # write test for do hold
        self.assertEqual(army_object_A.status_final, "Austin")

        # eval game
        # tests find conflicts and resolve conflicts internally
        game_object.eval_game()
        self.assertEqual(army_object_A.status_final, "Austin")

    def test_game_methods_3(self):
        # set up
        game_object = Game()
        army_object_A = Army("A", "Austin", "Support", "B")
        game_object.add_army(army_object_A)
        army_object_B = Army("B", "Dallas", "Move", "Houston")
        game_object.add_army(army_object_B)
        army_object_C = Army("C", "FortWorth", "Move", "Galveston")
        game_object.add_army(army_object_C)
        army_object_D = Army("D", "ElPaso", "Move", "Austin")
        game_object.add_army(army_object_D)
        army_object_E = Army("E", "Galveston", "Support", "D")
        game_object.add_army(army_object_E)
        army_object_F = Army("F", "FortStockten", "Move", "Galveston")
        game_object.add_army(army_object_F)
        self.assertEqual(game_object.army_list, [army_object_A, army_object_B, army_object_C,
                                                 army_object_D, army_object_E, army_object_F])

        # run actions
        game_object.run_actions()
        # tests do_hold internally
        self.assertEqual(army_object_A.status_final, "Austin")
        self.assertEqual(army_object_B.support, [army_object_A])
        # tests do_move internally
        self.assertEqual(army_object_B.status_final, "Houston")
        self.assertEqual(army_object_C.status_final, "Galveston")
        self.assertEqual(army_object_F.status_final, "Galveston")
        # tests do_support internally
        self.assertEqual(army_object_D.status_final, "Austin")
        self.assertEqual(army_object_D.support, [army_object_E])
        self.assertEqual(army_object_E.status_final, "Galveston")

        # eval game
        # tests find conflicts and resolve conflicts internally
        game_object.eval_game()
        self.assertEqual(army_object_A.status_final, "[dead]")
        self.assertEqual(army_object_B.status_final, "Houston")
        self.assertEqual(army_object_B.support, [army_object_A])
        self.assertEqual(army_object_C.status_final, "[dead]")
        self.assertEqual(army_object_D.status_final, "[dead]")
        self.assertEqual(army_object_D.support, [])
        self.assertEqual(army_object_E.status_final, "[dead]")
        self.assertEqual(army_object_F.status_final, "[dead]")

    # -------------------------------------------------------
    # army class tests
    # -------------------------------------------------------

    # -----------
    # create army
    # -----------

    def test_create_army_1(self):
        army_object = Army("A", "Madrid", "Hold")
        self.assertEqual(army_object.army_name, "A")
        self.assertEqual(army_object.status_initial, "Madrid")
        self.assertEqual(army_object.action, "Hold")
        self.assertEqual(army_object.action_target, None)
        self.assertEqual(army_object.support, [])
        self.assertEqual(army_object.status_final, None)

    def test_create_army_2(self):
        army_object = Army("B", "Barcelona", "Move", "Madrid")
        self.assertEqual(army_object.army_name, "B")
        self.assertEqual(army_object.status_initial, "Barcelona")
        self.assertEqual(army_object.action, "Move")
        self.assertEqual(army_object.action_target, "Madrid")
        self.assertEqual(army_object.support, [])
        self.assertEqual(army_object.status_final, None)

    def test_create_army_3(self):
        army_object = Army("D", "Paris", "Support", "B")
        self.assertEqual(army_object.army_name, "D")
        self.assertEqual(army_object.status_initial, "Paris")
        self.assertEqual(army_object.action, "Support")
        self.assertEqual(army_object.action_target, "B")
        self.assertEqual(army_object.support, [])
        self.assertEqual(army_object.status_final, None)

    # ------------
    # army methods
    # ------------

    def test_army_method(self):
        # getters
        army_object = Army("B", "Barcelona", "Move", "Madrid")
        self.assertEqual(army_object.get_name(), "B")
        self.assertEqual(army_object.get_initial(), "Barcelona")
        self.assertEqual(army_object.get_action(), "Move")
        self.assertEqual(army_object.get_action_target(), "Madrid")
        self.assertEqual(army_object.get_support(), [])
        self.assertEqual(army_object.get_strength(), 0)
        self.assertEqual(army_object.get_final(), None)
        self.assertEqual(army_object.get_conflicts(), [])

        # set final status
        self.assertEqual(army_object.status_final, None)
        army_object.set_final_status("Madrid")
        self.assertEqual(army_object.status_final, "Madrid")

        # add support
        army_object_supporter = Army("D", "Paris", "Support", "B")
        self.assertEqual(army_object.support, [])
        army_object.add_support(army_object_supporter)
        self.assertEqual(army_object.support, [army_object_supporter])

        # add conflict
        army_object_conflict = Army("A", "Londen", "Move", "Paris")
        self.assertEqual(army_object_conflict.conflicts, [])
        army_object_conflict.add_conflict(army_object_supporter)
        self.assertEqual(army_object_conflict.conflicts,
                         [army_object_supporter])

        # invalidate support
        army_object.invalidate_support(army_object_supporter)
        self.assertEqual(army_object.support, [])

    # -------------------------------------------------------
    # general function tests
    # -------------------------------------------------------

    # ----
    # read
    # ----

    def test_read_hold(self):
        s = "A Madrid Hold\n"
        i, j, k, m = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")
        self.assertEqual(m, None)

    def test_read_move(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, m = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, "Move")
        self.assertEqual(m, "Madrid")

    def test_read_support(self):
        s = "C London Support B\n"
        i, j, k, m = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "London")
        self.assertEqual(k, "Support")
        self.assertEqual(m, "B")

    # -----
    # print
    # -----
    def test_print_dead(self):
        w = StringIO()
        diplomacy_print(w, "A", "[dead]")
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_alive(self):
        w = StringIO()
        diplomacy_print(w, "B", "Madrid")
        self.assertEqual(w.getvalue(), "B Madrid\n")

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO("A Tokyo Move Berlin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Berlin\n")

    def test_solve_3(self):
        r = StringIO(
            "A HongKong Support B\nB Tokoyo Move HongKong\nC MexicoCity Move HongKong\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Berlin Move Rome\nB Rome Move Ankara\nC Ankara Move Cairo\nD Cairo Move Berlin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Rome\nB Ankara\nC Cairo\nD Berlin\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()


""" #pragma: no cover

................
----------------------------------------------------------------------
Ran 16 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         118      0     34      0   100%
TestDiplomacy.py     162      0      0      0   100%
--------------------------------------------------------------
TOTAL                280      0     34      0   100%


"""
