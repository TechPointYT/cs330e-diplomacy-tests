#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "C London Support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["C", "London", "Support", "B"])

    def test_read_3(self):
        s = "Y Paris Move Dallas\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["Y", "Paris", "Move", "Dallas"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"], ["E", "Austin", "Support", "A"]])
        self.assertEqual(v, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Austin'})

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Toronto", "Move", "London"], ["B", "Paris", "Move", "London"], ["C", "London", "Support", "A"]])
        self.assertEqual(v, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]'})

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Support", "C"], ["B", "Barcelona", "Hold"], ["C", "London", "Move", "Madrid"]])
        self.assertEqual(v, {'A': '[dead]', 'B': 'Barcelona', 'C': '[dead]'})

    # -----
    # print
    # -----


    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Austin'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]', 'B': 'Barcelona', 'C': 'London'})
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC London\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C\nE Dallas Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin\nE Dallas\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC Dallas Hold\nD Austin Move Chicago\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC Dallas\nD Chicago\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()